#include <string.h>
#include "../src/string.h"
#include "test.h"

void test_string(void) {
    string_t *string;
    const char *str_c;

    string = string_init_cstr("hello", 5);
    assert(string != NULL);

    str_c = string_to_cstr(string);
    assert(strcmp(str_c, "hello") == 0);

    assert(string_len(string) == 5);

    char val = 'a';
    string = string_insert_one(string, 2, val);
    str_c = string_to_cstr(string);
    assert(strcmp(str_c, "heallo") == 0);

    string_destroy(string);
}

int main(void) {
    test_string();
}
