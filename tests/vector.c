#include "../src/vector.h"
#include "test.h"

void test_vector(void) {
    vector_t *vector;

    vector = vector_init(sizeof(int));
    assert(vector != NULL);

    int u[] = {1, 2, 3, 10, 20, 30, 300, 200, 100};
    int size = sizeof(u)/sizeof(int);
    vector = vector_append_many(vector, u, size);
    assert(vector != NULL);
    assert(vector_size(vector) == size);

    int k = 33;
    vector = vector_append_one(vector, k);
    assert(vector_size(vector) == size+1);
    size++;

    vector_delete_one(vector, 1);
    assert(vector_size(vector) == size-1);
    assert(vector_get(vector, int, 0) == 1);
    assert(vector_get(vector, int, 1) == 3);
    size--;

    vector_delete_one(vector, 0);
    assert(vector_size(vector) == size-1);
    assert(vector_get(vector, int, 0) == 3);
    assert(vector_get(vector, int, 1) == 10);
    size--;

    /* 3 10 20 30 300 200 100 33 */
    vector = vector_insert_one(vector, 2, k);
    assert(vector_size(vector) == size+1);
    assert(vector_get(vector, int, 1) == 10);
    assert(vector_get(vector, int, 2) == k);
    assert(vector_get(vector, int, 3) == 20);
    size++;

    /* 3 10 33 20 30 300 200 100 33 */
    int kk = 700;
    vector_set(vector, 5, kk);
    assert(vector_get(vector, int, 5) == kk);

    vector_destroy(vector);
}

int main(void) {
    test_vector();
}
