#pragma once

#define assert(e) do { \
    if (!(e)) { \
        assert_failed(__func__, #e, __FILE__, __LINE__); \
    } \
} while(0);

void assert_failed(const char *, const char *, const char *, const int);
