#include <stdio.h>
#include "vector.h"
#include "string.h"
#include "file.h"
#include "screen.h"

#include "action.h"

/*
 * Inserts a newline under cursor, which means that the
 * current line will be split to two. Called when the
 * user presses the enter key.
 */
void insert_newline_char(void)
{
    int line_num, col_num;

    if ( file->read_only ) {
        screen_set_status("File is open for reading only!");
        return ;
    }

    screen_get_file_pos(&line_num, &col_num);

    string_t *line = vector_get(file->lines, string_t *, line_num - 1);
    string_t *newline = string_init_cstr(string_to_cstr(line) + col_num - 1,
            string_len(line) - (col_num - 1));

    string_delete_many(line, col_num - 1, string_len(line) - (col_num - 1));
    file->lines = vector_insert_one(file->lines, line_num, newline);
    screen_update_bounds();

    screen_move_cursor(1, 0);
    refresh();
    screen_move_cursor_to_start_of_line();
    refresh();

    screen_redraw();
}

/*
 * Deletes the character under cursor. Called when the
 * user hits the backspace key.
 */
void delete_char(void)
{
    int line_num, col_num;
    string_t *line, *prev_line;

    if ( file->read_only ) {
        screen_set_status("File is open for reading only!");
        return ;
    }

    screen_get_file_pos(&line_num, &col_num);

    line = vector_get(file->lines, string_t *, line_num - 1);

    /* If the cursor is at the start of line, we need to
     * concatenate it with the previous line. */
    if ( col_num == 1 ) {
        if ( line_num == 1 ) return ;

        /* Jump to the end of the previous line. */
        screen_move_cursor(-1, 0);
        refresh();
        screen_move_cursor_to_end_of_line();
        refresh();

        /* Concatenate the current line with the previous. */
        prev_line = vector_get(file->lines, string_t *, line_num - 2);
        prev_line = string_concat(prev_line, line);
        vector_set(file->lines, line_num - 2, prev_line);

        vector_delete_one(file->lines, line_num - 1);

        /* TODO: update screen bounds */
        screen_redraw();
    }
    else {
        string_delete_one(line, col_num - 2);

        screen_redraw_line();
        screen_move_cursor(0, -1);
        refresh();
    }
}

void insert_char(int key)
{
    int line_num, col_num;
    string_t *line;

    if ( file->read_only ) {
        screen_set_status("File is open for reading only!");
        return ;
    }

    screen_get_file_pos(&line_num, &col_num);

    line = vector_get(file->lines, string_t *, line_num - 1);
    line = string_insert_one(line, col_num - 1, key);
    vector_set(file->lines, line_num - 1, line);

    screen_redraw_line();
    screen_move_cursor(0, 1);
    refresh();
}
