#include <stdio.h>
#include <string.h>
#include <sys/errno.h>
#include <stdarg.h>

#include "common.h"

extern int errno;

int min(int a, int b)
{
    return (a < b ? a : b);
}

int max(int a, int b)
{
    return (a > b ? a : b);
}

int debug(const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    fprintf(stderr, "DEBUG: ");
    fprintf(stderr, fmt, args);
    va_end(args);
}

void strperror(char *buf, const char *str)
{
    strcpy(buf, str);
    strcat(buf, ": "); 
    strcat(buf, strerror(errno));
}
