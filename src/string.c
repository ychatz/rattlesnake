#include "vector.h"
#include "string.h"

string_t * string_init(void)
{
    char null = '\0';
    string_t *string;

    string = vector_init(1);
    string = vector_append_one(string, null);

    return string;
}

string_t * string_init_cstr(const char *cstr, int len)
{
    char null = '\0';
    string_t *string;

    string = vector_init(1);
    if ( len > 0 )
        string = vector_append_many(string, cstr, len);

    string = vector_append_one(string, null);

    return string;
}

void string_destroy(string_t *string)
{
    vector_destroy(string);
}

int string_len(string_t *string)
{
    /* subtract the null terminator from the result */
    return vector_size(string) - 1;
}

const char *string_to_cstr(string_t *string)
{
    return vector_to_carr(string);
}

void string_delete_many(string_t *string, int pos, int count)
{
    vector_delete_many(string, pos, count);

}
void string_delete_one(string_t *string, int pos)
{
    vector_delete_one(string, pos);
}

string_t *string_insert_one(string_t *string, int pos, char val)
{
    return vector_insert_one(string, pos, val);
}

string_t *string_concat(string_t *string, string_t *string2)
{
    /* drop the null terminator */
    vector_pop_back(string, 1);

    /* concatenate and return */
    return vector_append_many(string, string_to_cstr(string2),
            vector_size(string2));
}

string_t *string_concat_cstr(string_t *string, char *string2, int len)
{
    char null = '\0';

    /* drop the null terminator */
    vector_pop_back(string, 1);

    /* concatenate */
    string = vector_append_many(string, string2, len);

    /* append the null terminator */
    string = vector_append_one(string, null);

    return string;
}
