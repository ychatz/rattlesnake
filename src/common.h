#pragma once

int  min       (int, int);
int  max       (int, int);
int  debug     (const char *, ...);
void strperror (char *, const char *);
