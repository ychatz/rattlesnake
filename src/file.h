#pragma once

#include "vector.h"
#include "string.h"

struct file_t {
    /* the file contents represented as a vector of one string_t per line */
    vector_t *lines;
    /* the filename that is being edited */
    char *filename;
    /* the file descriptor of the opened file */
    int fd;
    /* flags */
    unsigned read_only : 1;
    unsigned modified: 1;
};
typedef struct file_t file_t;

extern file_t *file;

void file_init    (char *);
void file_destroy (void);
