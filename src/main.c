#include <stdlib.h>
#include <signal.h>
#include <ncurses.h>

#include "action.h"
#include "common.h"
#include "file.h"
#include "screen.h"

void destroy(void)
{
    file_destroy();
    screen_destroy();
}

void parse_args(int argc, char **argv)
{
    file_init(argc > 1 ? argv[1] : "");
}

int main(int argc, char **argv)
{
    int i;
    int line_count;
    char err[256];

    parse_args(argc, argv);

    screen_init();

    while(1) {
        refresh();

        int key = getch();
        if ( key == erasechar() )
            key = KEY_BACKSPACE;

        screen_set_status("");
        switch(key) {
            case KEY_LEFT:
                screen_move_cursor(0, -1);
                break;
            case KEY_RIGHT:
                screen_move_cursor(0, +1);
                break;
            case KEY_UP:
                screen_move_cursor(-1, 0);
                break;
            case KEY_DOWN:
                screen_move_cursor(+1, 0);
                break;
            case 19: /* CTRL+S */
                screen_set_status("Saving...");
                if ( save_file() < 0 ) {
                    strperror(err, "Save failed");
                    screen_set_status(err);
                }
                else
                    screen_set_status("Saved");
                break;
            case 1: /* CTRL+A */
                screen_move_cursor_to_start_of_line();
                break;
            case 5: /* CTRL+E */
                screen_move_cursor_to_end_of_line();
                break;
            case '\r':
            case '\n':
            case KEY_ENTER:
                insert_newline_char();
                break;
            case KEY_BACKSPACE:
                delete_char();
                break;
            case KEY_RESIZE: /* Received SIGWINCH */
                screen_update_bounds();
                screen_redraw();
                break;
            default:
                if ( key < 0 ) continue;
                insert_char(key);
                break;
        }
    }

    destroy();

    return 0;
}
