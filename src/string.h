#pragma once

#include "vector.h"

typedef struct vector_t string_t;

string_t   * string_init           (void);
string_t   * string_init_cstr      (const char *, int);
void         string_destroy        (string_t *);
int          string_len            (string_t *);
const char * string_to_cstr        (string_t *);
string_t   * string_concat         (string_t *, string_t *);
string_t   * string_concat_cstr    (string_t *, char *, int);
string_t   * string_insert_one     (string_t *, int, char);
