#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>

#include "string.h"
#include "file.h"

file_t *file;

string_t *read_file(void)
{
#define READ_BUF_SIZE 512
    char buf[READ_BUF_SIZE];
    string_t *contents;
    int ret;

    contents = string_init();

    while ((ret = read(file->fd, buf, READ_BUF_SIZE)) > 0) {
        contents = string_concat_cstr(contents, buf, ret);
    }

    if ( ret < 0 ) {
        perror("read");
        exit(1);
    }

    return contents;
}

int insist_write(const char *str, int len)
{
    int pos = 0, ret;

    while (pos < len && (ret = write(file->fd, str + pos, len - pos)) > 0)
        pos += ret;

    if ( ret < 0 )
        return ret;

    return 0;
}

/* Tries to save the file and returns 0 if it succeeds or -1 if it fails. */
int save_file(void)
{
    int i, line_count;
    const char *line_cstr;
    string_t *line;

    /* First erase the contents of the file. */
    if ( ftruncate(file->fd, 0) < 0 )
        return -1;

    /* Then write our data. */
    line_count = vector_size(file->lines);
    for (i=0; i<line_count; ++i) {
        line = vector_get(file->lines, string_t *, i);
        line_cstr = string_to_cstr(line);

        if ( insist_write(line_cstr, string_len(line)) < 0 )
            return -1;
        if ( insist_write("\n", 1) < 0 )
            return -1;
    }

    file->modified = 0;
    return 0;
}

void split_contents_to_lines(string_t *contents)
{
    int i, j, len, k;
    const char *str;
    string_t *newline;
    char null = '\0';

    len = string_len(contents);
    str = string_to_cstr(contents);
    for (i=0; i<len; i=j+1) {
        for (j=i; j<len && str[j] != '\n'; ++j) ;
        newline = string_init_cstr(str+i, j-i);
        file->lines = vector_append_one(file->lines, newline);
    }
}

void file_init(char *filename)
{
    /* Initialize the file structure */
    file = malloc(sizeof(file_t));

    /* Open the file */
    if ( filename[0] != '\0' ) {
        file->filename = filename;
        file->fd = open(filename, O_RDWR | O_APPEND | O_CREAT, 0644);
        file->read_only = 0;
    }
    else {
        file->filename = "stdin";
        file->fd = 0;
        file->read_only = 1;
    }

    /* If `open` failed, try to open it for reading only */
    if ( file->fd < 0 && errno == EACCES ) {
        file->fd = open(filename, O_RDONLY);
        file->read_only = 1;
    }

    if ( file->fd < 0 ) {
        perror("open");
        exit(1);
    }

    /* Read the file contents into `contents` */
    string_t *contents;
    contents = read_file();

    /* Tokenize `contents` to find the file lines */
    file->lines = vector_init(sizeof(string_t *));
    split_contents_to_lines(contents);

    file->modified = 0;

    string_destroy(contents);
}

void file_destroy(void)
{
    if ( file->fd > 0 )
        close(file->fd);

    vector_destroy(file->lines);
}
