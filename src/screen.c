#include <ncurses.h>

#include "file.h"
#include "vector.h"
#include "string.h"
#include "common.h"

#include "screen.h"

/* first and last visible lines of the opened file */
static int first_line = 1, last_line;
/* first and last visible columns */
static int first_column = 1, last_column;

static char *statusbar = "";

void screen_destroy(void)
{
    endwin();
}

void screen_init(void)
{
    initscr();
    start_color();
    /* raw(); */
    noecho();
    nonl();
    keypad(stdscr, TRUE);

    move(0, 5);
    refresh();

    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    init_pair(2, COLOR_RED, COLOR_BLACK);
    init_pair(3, COLOR_BLACK, COLOR_MAGENTA);
    init_pair(4, COLOR_RED, COLOR_BLACK);

    screen_redraw();
}

void screen_get_file_pos(int *line_num, int *col_num)
{
    int x, y;

    getsyx(y, x);

    *line_num = first_line + y;
    *col_num = first_column + x - 5;
}

void screen_update_bounds(void)
{
    int line_count;
    int x, y;

    /* Update the last_line and last_column values. */
    line_count = min(LINES-1, vector_size(file->lines));
    last_line = first_line + line_count - 1;
    last_column = COLS-5;

    /* Reposition the cursor if need be: If the line that the cursor was placed
     * is hidden after the resize, the cursor will automatically be placed at
     * the last line of the terminal which is now the status line. In this case
     * we move it one line upwards. */
    getsyx(y, x);
    if ( y == LINES-1 ) {
        move(y-1, x);
        refresh();
    }
}

void screen_redraw_line(void)
{
    int x, y;
    string_t *line;

    curs_set(0);
    refresh();
    getsyx(y, x);
    move(y, 5);
    refresh();

    line = vector_get(file->lines, string_t *, first_line + y - 1);
    mvwprintw(stdscr, y, 5, "%s", string_to_cstr(line));
    clrtoeol();

    move(y, x);
    curs_set(1);
    refresh();
}

void screen_draw_file_contents(void)
{
    int i;

    attron(COLOR_PAIR(1));
    for (i=first_line ; i<=last_line; ++i) {
        move(i - first_line, 5);
        clrtoeol();
        mvwprintw(stdscr, i - first_line, 5, "%s", 
                string_to_cstr(vector_get(file->lines, string_t *, i-1)));
    }
}

void screen_draw_line_numbers(void)
{
    int i;

    attron(COLOR_PAIR(2));
    for (i = first_line; i <= last_line; ++i) {
        mvwprintw(stdscr, i - first_line, 0, "%4d ", i);
    }
}

void screen_draw_status_line(void)
{
    int i;
    string_t *file_str;

    /* draw the background (also cleans any previous text) */
    attron(COLOR_PAIR(3));
    for (i = 0; i < COLS; ++i) {
        mvwprintw(stdscr, LINES-1, i, " ");
    }

    /* draw the statusbar */
    mvwprintw(stdscr, LINES-1, 5, "%s", statusbar);

    /* draw the filename */
    attron(COLOR_PAIR(4));

    file_str = string_init_cstr(file->filename, strlen(file->filename));
    if ( file->read_only )
        file_str = string_concat_cstr(file_str, " [RO]", 5);

    mvwprintw(stdscr, LINES-1, COLS - string_len(file_str) - 5, " %s ",
            string_to_cstr(file_str));

    string_destroy(file_str);

    attron(COLOR_PAIR(1));
}

void screen_set_status(char *status)
{
    int x, y;

    getsyx(y, x);

    statusbar = status;
    screen_draw_status_line();

    move(y,x);
    refresh();
}

void screen_scroll_v(int dy)
{
    if ( first_line + dy < 1 )
        dy = -(first_line - 1);
    if ( last_line + dy > vector_size(file->lines) )
        dy = vector_size(file->lines) - last_line;

    first_line += dy;
    last_line += dy;

    screen_redraw();
}

void screen_scroll_h(int dx) /* TODO: fix this */
{
    first_column = max(first_column + dx, 1);
    last_column = first_column + COLS - 5;

    screen_redraw();
}

void screen_move_cursor(int dy, int dx)
{
    int x, y;

    getsyx(y, x);
    y += dy;
    x += dx;

    int line_num = first_line + y;
    int col_num = first_column + x - 5 - 1;

    if ( line_num > vector_size(file->lines) || line_num < 1 )
        return ;
    
    string_t *line = vector_get(file->lines, string_t *, line_num-1);

    if ( col_num > string_len(line) )
        x -= col_num - string_len(line);

    if ( x < 5 )
        screen_scroll_h(dx);
    else if ( x > COLS )
        screen_scroll_h(dx);
    else if ( y < 0 )
        screen_scroll_v(dy);
    else if ( y > LINES-2 )
        screen_scroll_v(dy);
    else
        move(y, x);
}

void screen_move_cursor_to_start_of_line(void)
{
    int x, y, col_num;

    getsyx(y, x);
    col_num = first_column + x - 5 - 1;

    screen_move_cursor(0, -col_num);
}

void screen_move_cursor_to_end_of_line(void)
{
    int x, y;

    /* Get the current position. */
    getsyx(y, x);

    /* Calculate the final position. */
    int line_number = first_line + y;
    string_t *line = vector_get(file->lines, string_t *, line_number-1);
    int x2 = string_len(line);

    /* Move the cursor relatively to the current position. */
    screen_move_cursor(0, x2 - x + 5);
}

void screen_redraw(void)
{
    int x, y;

    getsyx(y, x);

    screen_update_bounds();

    screen_draw_line_numbers();
    screen_draw_status_line();
    screen_draw_file_contents();

    refresh();

    move(y, x);
}
