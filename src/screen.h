#pragma once

void screen_init                         (void);
void screen_destroy                      (void);
void screen_set_bounds                   (int, int, int, int);
void screen_update_bounds                (void);
void screen_set_status                   (char *);
void screen_draw_status_line             (void);
void screen_draw_line_numbers            (void);
void screen_redraw                       (void);
void screen_move_cursor                  (int, int);
void screen_move_cursor_to_end_of_line   (void);
void screen_move_cursor_to_start_of_line (void);
void screen_get_file_pos                 (int *, int *);
