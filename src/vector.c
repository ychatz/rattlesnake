#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "vector.h"

vector_t *vector_init(size_t elem_size)
{
    vector_t *vector;
#define VECTOR_RESERVE_ON_INIT 32
    vector = malloc(sizeof(vector_t) + VECTOR_RESERVE_ON_INIT * elem_size);
    if (vector == NULL)
        return NULL;

    vector->reserved = VECTOR_RESERVE_ON_INIT;
    vector->len = 0;
    vector->elem_size = elem_size;

    return vector;
}

void vector_destroy(vector_t *vector)
{
    free(vector);
}

vector_t *vector_expand(vector_t *vector, int extra)
{
    int new_len;

    new_len = vector->len + extra * vector->elem_size;

    while (new_len > vector->reserved) {
        vector->reserved *= 2;
        vector = realloc(vector, sizeof(vector_t) +
                vector->reserved);
        if (vector == NULL)
            return NULL;
    }

    return vector;
}


void vector_delete_many(vector_t *vector, int pos, int len)
{
    int del_bytes;

    del_bytes = len * vector->elem_size;

    memmove(vector->data + pos * vector->elem_size,
            vector->data + pos * vector->elem_size + del_bytes,
            (vector_len(vector) - pos) * vector->elem_size - del_bytes);

    vector->len -= del_bytes;
}

vector_t *vector_insert_many(vector_t *vector, int pos,
        const void *elem_arr, int len)
{
    int new_bytes;

    vector = vector_expand(vector, len);
    new_bytes = len * vector->elem_size;

    memmove(vector->data + pos * vector->elem_size + new_bytes,
            vector->data + pos * vector->elem_size,
            (vector_len(vector) - pos) * vector->elem_size);

    memmove(vector->data + pos * vector->elem_size,
            elem_arr,
            new_bytes);

    vector->len += new_bytes;

    return vector;
}

vector_t *vector_append_many(vector_t *vector, const void *elem_arr, int len)
{
    vector = vector_expand(vector, len);

    memcpy(vector->data + vector->len, elem_arr, len * vector->elem_size);
    vector->len += len * vector->elem_size;

    return vector;
}

void vector_pop_back(vector_t *vector, int count)
{
    vector->len -= count * vector->elem_size;
}
