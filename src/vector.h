#pragma once

#include <string.h>
#include <stddef.h>

struct vector_t {
    int reserved;
    int len; /* length in bytes */
    size_t elem_size;
    char data[0];
};
typedef struct vector_t vector_t;

vector_t * vector_init           (size_t);
void       vector_destroy        (vector_t *);
vector_t * vector_append_many    (vector_t *, const void *, int);
vector_t * vector_insert_many    (vector_t *, int, const void *, int);
void       vector_delete_many    (vector_t *, int, int);
void       vector_pop_back       (vector_t *, int);

#define vector_append_one(vector, elem)                                        \
    vector_append_many(vector, &(elem), 1)

#define vector_delete_one(vector, ind)                                         \
    vector_delete_many(vector, ind, 1)

#define vector_insert_one(vector, ind, elem)                                   \
    vector_insert_many(vector, ind, &(elem), 1)

#define vector_size(vector)                                                    \
    ((vector)->len / (vector)->elem_size)

#define vector_len(vector)                                                     \
    vector_size(vector)

#define vector_to_carr(vector)                                                 \
    (vector)->data

#define vector_get(vector, type, index)                                        \
    ((type *)(vector->data))[index]

#define vector_set(vector, index, new_elem)                                    \
    memcpy((vector)->data + (index) * (vector)->elem_size, &(new_elem),          \
            (vector)->elem_size)
