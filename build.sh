#!/usr/bin/env bash

cd src
cat <<EOT > Makefile
SOURCES:=\$(shell find . -name "*.c" -maxdepth 1)
DEPFILES:=\$(patsubst ./%.c,%.o,\$(SOURCES))

rattlesnake: \$(DEPFILES)
	gcc $^ -o \$@ -lncurses
	mv rattlesnake ../rattlesnake

clean:
	rm *.o
EOT

for source in *.c; do
    obj=`sed 's/c$/o/' <(echo -n $source)`
    gcc -MM $source >> Makefile
    echo "	gcc -c $source -o $obj" >> Makefile
done

make
